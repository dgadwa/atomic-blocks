/**
 * BLOCK: CTA Block.
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */


//  Import CSS.
import './style.scss';
import './editor.scss';
import { CTAIcon } from '../../icons'
import ButtonEdit from '../button/button-edit'
import ButtonSave from '../button/button-save'
import { deprecatedSave_0_7 } from './deprecated'

import {
	registerBlockType,
	__,
	PanelColorSettings,
	Dashicon,
	IconButton,
	SelectControl,
	RangeControl,
	InspectorControls,
	RichText,
	ColorPalette,
	PanelBody,
	URLInput,
} from '../../wp-imports'

export const edit = ( props ) => {

	const {
		isSelected,
		editable,
		setState,
		className,
		setAttributes
	} = props;

	const {
		url,
		buttonText,
		ctaTitle,
		bodyText,
		color,
		textColor,
		size,
		borderButtonRadius,
		bodyTextColor,
		titleColor,
		bgColor,
	} = props.attributes;

	const linkOptions = [
		{ value: 'small', label: __( 'Small' ) },
		{ value: 'normal', label: __( 'Normal' ) },
		{ value: 'medium', label: __( 'Medium' ) },
		{ value: 'large', label: __( 'Large' ) },
	];

	const onSetActiveEditable = ( newEditable ) => () => {
		setState( { editable: newEditable } )
	}

	return [
		isSelected && (
			<InspectorControls key={ 'inspector' }>
				<PanelBody>
					<SelectControl
						label={ __( 'Button Size' ) }
						value={ size }
						options={ linkOptions.map( ({ value, label }) => ( {
							value: value,
							label: label,
						} ) ) }
						onChange={ ( newSize ) => { setAttributes( { size: newSize } ) } }
					/>
					<RangeControl
						label={ __( 'Button Border Radius' ) }
						value={ borderButtonRadius }
						min='1'
						max='50'
						onChange={ ( borderRad ) => setAttributes( { borderButtonRadius: borderRad } ) }
					/>
				</PanelBody>
				<PanelColorSettings
					initialOpen={ false }
					title={ __( 'Color Settings' ) }
					colorSettings={ [
						{
							value: bgColor,
							onChange: ( colorValue ) => setAttributes( { bgColor: colorValue } ),
							label: __( 'Background Color' ),
						},
						{
							value: titleColor,
							onChange: ( colorValue ) => setAttributes( { titleColor: colorValue } ),
							label: __( 'Title Color' ),
						},
						{
							value: bodyTextColor,
							onChange: ( colorValue ) => setAttributes( { bodyTextColor: colorValue } ),
							label: __( 'Body Text Color' ),
						},
					] }
				>
				</PanelColorSettings>
				<PanelColorSettings
				title={ __( 'Button Colors' ) }
				colorSettings={ [
						{
							value: color,
							onChange: ( colorValue ) => setAttributes( { color: colorValue } ),
							label: __( 'Button Background Color' ),
						},
						{
							value: textColor,
							onChange: ( colorValue ) => setAttributes( { textColor: colorValue } ),
							label: __( 'Button Text Color' ),
						},
					] }
				>
				</PanelColorSettings>
			</InspectorControls>
		),
		<div key={'editable'} className={ 'ugb-cta' } style={ { backgroundColor: bgColor } }>
			<RichText
				className={ 'ugb-cta-title' }
				tagName={ 'h3' }
				placeholder={ __('Add Title') }
				value={ ctaTitle }
				onChange={ (text) => setAttributes( { ctaTitle: text } ) }
				isSelected={ isSelected && editable === 'ctaTitle' }
				onFocus={ onSetActiveEditable( 'ctaTitle' ) }
				keepPlaceholderOnFocus
				style={ {
					color: titleColor,
				} }
			/>
			<RichText
				tagName={'p'}
				value={ bodyText }
				className={ 'ugb-cta-bodyText' }
				onChange={ (text) => setAttributes( { bodyText: text } ) }
				isSelected={ isSelected && editable === 'bodyText' }
				onFocus={ onSetActiveEditable( 'bodyText' ) }
				placeholder={ __( 'Write body text…' ) }
				style={ {
					color: bodyTextColor,
				} }
			/>
			<ButtonEdit size={ size } color={ textColor } backgroundColor={ color } text={ buttonText } borderRadius={ borderButtonRadius } 
				onChange={ (text) => setAttributes( { buttonText: text } ) }
				isSelected={ isSelected && editable === 'buttonText' }
				onFocus={ onSetActiveEditable( 'buttonText' ) }
				/>
		</div>,
		isSelected && (
			<form
				key={ 'form-link' }
				onSubmit={ ( event ) => event.preventDefault() }
				className={ `blocks-button__inline-link`}>
				<Dashicon icon={ 'admin-links' } />
				<URLInput
					value={ url }
					onChange={ ( value ) => setAttributes( { url: value } ) }
				/>
				<IconButton
					icon={ 'editor-break' }
					label={ __( 'Apply' ) }
					type={ 'submit' }
				/>
			</form>
		)
	];
}

export const save = ( props ) => {

	const {
		url,
		buttonText,
		ctaTitle,
		bodyText,
		color,
		textColor,
		size,
		borderButtonRadius,
		bodyTextColor,
		titleColor,
		bgColor,
	} = props.attributes;

	return (
		<div className={ `ugb-cta` } style={ { backgroundColor: bgColor } }>
			{ ctaTitle && !! ctaTitle.length && (
				<h3
					className={ 'ugb-cta-title' }
					style={ { color: titleColor } }>
					{ctaTitle}
				</h3>
			) }
			{ bodyText && !! bodyText.length && (
				<p
					className={ 'ugb-cta-bodyText' }
					style={ { color: bodyTextColor } }>
					{bodyText}
				</p>
			) }
			{ buttonText && !! buttonText.length && (
				<ButtonSave size={ size } url={ url } color={ textColor } text={ buttonText } backgroundColor={ color } borderRadius={ borderButtonRadius } />
			) }
		</div>
	);
}

const schema = {
	url: {
		type: 'string',
		source: 'attribute',
		selector: 'a',
		attribute: 'href',
	},
	ctaTitle: {
		type: 'array',
		source: 'children',
		selector: 'h3',
		default: __( 'Get Started Today' )
	},
	bodyText: {
		type: 'array',
		source: 'children',
		selector: 'p',
		default: __( 'Get Stackable: Ultimate Gutenberg Blocks today.  Apart from adding new blocks, it gives Gutenberg users more options and settings to tinker with, expanding Gutenberg’s functionality.' )
	},
	buttonText: {
		type: 'array',
		source: 'children',
		selector: 'a',
	},
	color: {
		type: 'string',
		default: '#2091e1',
	},
	textColor: {
		type: 'string',
		default: '#ffffff',
	},
	titleColor: {
		type: 'string',
	},
	bodyTextColor: {
		type: 'string',
	},
	bgColor: {
		type: 'string',
	},
	size: {
		type: 'string',
		default: 'normal',
	},
	borderButtonRadius: {
		type: 'number',
		default: 4,
	},
}

/**
 * Register: Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'ugb/cta', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Call to Action' ), // Block title.
	icon: CTAIcon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'stackable-ultimate-gutenberg-blocks', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'Call to Action' ),
		__( 'Stackable' ),
		__( 'CTA' ),
	],
	attributes: schema,
	supports: {
		align: true,
		align: [ 'center', 'wide', 'full' ],
	},
	deprecated: [
        {
			save: deprecatedSave_0_7,
        }
    ],

	edit,
	save,
} );
