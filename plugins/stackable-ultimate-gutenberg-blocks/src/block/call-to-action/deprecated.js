export const deprecatedSave_0_7 = ( props ) => {

	const {
		url,
		buttonText,
		ctaTitle,
		bodyText,
		color,
		textColor,
		size,
		borderButtonRadius,
		bodyTextColor,
		titleColor,
		bgColor
	} = props.attributes;

	const buttonStyle = {
		backgroundColor: color,
		color: textColor,
		borderRadius: borderButtonRadius + 'px',
	}

	return (
		<div className={ `ugb-cta` } style={ { backgroundColor: bgColor } }>
			{ ctaTitle && !! ctaTitle.length && (
				<h3
					className={ 'ugb-cta-title' }
					style={ { color: titleColor } }>
					{ctaTitle}
				</h3>
			) }
			{ bodyText && !! bodyText.length && (
				<p
					className={ 'ugb-cta-bodyText' }
					style={ { color: bodyTextColor } }>
					{bodyText}
				</p>
			) }
			{ buttonText && !! buttonText.length && (
				<a
					href={ url }
					className={ `wp-ugb-button ugb-cta-button ugb-button-${size}` }
					style={ buttonStyle }>
					{ buttonText }
				</a>
			) }
		</div>
	);
}