function ButtonSave( props ) {
    const { 
        align = 'center', 
        size = 'normal', 
        url = '', color, text, backgroundColor, borderRadius } = props

    const buttonStyle = {
		backgroundColor: backgroundColor,
		borderRadius: borderRadius + 'px',
	}

	return (
		<div className={ `ugb-button ugb-button-${align} ugb-button-${size}` } style={ buttonStyle }>
			<a href={ url } className={ `ugb-button-inner` } style={ { color } }>
				{ text }
			</a>
		</div>
	);
}

export default ButtonSave