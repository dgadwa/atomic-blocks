import {
	__,
	RichText,
} from '../../wp-imports'

function ButtonEdit( props ) {
    const { 
        align = 'center', 
        size = 'normal', 
        color,
        text = '', 
        backgroundColor, 
        borderRadius = 4, 
        isSelected = false, 
        onFocus = () => {},
        onChange = () => {},
    } = props

    // const onFocus = editableName ? () => {
    //     onSetActiveEditable( editableName )
    // } : null
    
    return (
        <span
            className={ `ugb-button ugb-button-${align} ugb-button-${size}` }
            style={ {
                backgroundColor: backgroundColor,
                borderRadius: borderRadius + 'px',
            } }>
            <RichText
                tagName={ 'span' }
                placeholder={ __( 'Enter Text' ) }
                value={ text }
                onChange={ onChange }
                formattingControls={ [ 'bold', 'italic', 'strikethrough' ] }
                className={ `ugb-button-inner` }
                style={ { color } }
                onFocus={ onFocus }
                isSelected={ isSelected }
                keepPlaceholderOnFocus
            />
        </span>
    )
}

export default ButtonEdit