export const deprecatedSave_0_7 = ( props ) => {

	const { url, text, textAlignment, color, textColor, size, cornerButtonRadius } = props.attributes;

	const buttonStyle = {
		backgroundColor: color,
		color: textColor,
		borderRadius: cornerButtonRadius + 'px',
	}

	return (
		<div className={ `ugb-button-${textAlignment}` }>
			<a href={ url } className={ `wp-ugb-button ugb-button-${size}` } style={ buttonStyle }>
				{ text }
			</a>
		</div>
	);
}

export const deprecatedSchema_0_7 = {
	url: {
		type: 'string',
		source: 'attribute',
		selector: 'a',
		attribute: 'href',
	},
	text: {
		type: 'array',
		source: 'children',
		selector: 'a',
	},
	align: {
		type: 'string',
		default: 'center',
	},
	color: {
		type: 'string',
		default: '#2091e1',
	},
	textColor: {
		type: 'string',
		default: '#ffffff',
	},
	size: {
		type: 'string',
		default: 'normal',
	},
	cornerButtonRadius: {
		type: 'number',
		default: 4,
	}
}