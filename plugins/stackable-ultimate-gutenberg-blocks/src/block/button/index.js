/**
 * BLOCK: Button Block.
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';
import { ButtonIcon } from '../../icons'
import { deprecatedSave_0_7, deprecatedSchema_0_7 } from './deprecated'
import ButtonEdit from './button-edit'
import ButtonSave from './button-save'

import {
	registerBlockType, // Import registerBlockType() from wp.blocks
	__,
	InspectorControls,
	BlockControls,
	AlignmentToolbar,
	BlockAlignmentToolbar,
	RichText,
	ColorPalette,
	PanelColorSettings,
	Dashicon,
	IconButton,
	SelectControl,
	RangeControl,
	URLInput,
	PanelBody,
	omit,
	merge,
} from '../../wp-imports'

export const edit = ( props ) => {

	const { isSelected, className, setAttributes } = props;

	const { url, text, color, textColor, size, align, cornerButtonRadius } = props.attributes;

	const linkOptions = [
		{ value: 'small', label: __( 'Small' ) },
		{ value: 'normal', label: __( 'Normal' ) },
		{ value: 'medium', label: __( 'Medium' ) },
		{ value: 'large', label: __( 'Large' ) },
	];

	return [
		isSelected && (
			<BlockControls key="controls">
				<BlockAlignmentToolbar
					value={ align }
					onChange={ ( align ) => {
						setAttributes( { align } );
					} }
					controls={ [ 'left', 'center', 'right', 'full' ] }
				/>
			</BlockControls>
		),
		isSelected && (
			<InspectorControls key={ 'inspector' }>
				<PanelBody>
					<SelectControl
						label={ __( 'Size' ) }
						value={ size }
						options={ linkOptions.map( ({ value, label }) => ( {
							value: value,
							label: label,
						} ) ) }
						onChange={ ( newSize ) => { setAttributes( { size: newSize } ) } }
					/>
					<RangeControl
						label={ __( 'Corner Radius' ) }
						value={ cornerButtonRadius }
						min='1'
						max='50'
						onChange={ ( cornerRad ) => setAttributes( { cornerButtonRadius: cornerRad } ) }
					/>
				</PanelBody>


				<PanelColorSettings
					title={ __( 'Color Settings' ) }
					colorSettings={ [
						{
							value: color,
							onChange: ( colorValue ) => setAttributes( { color: colorValue } ),
							label: __( 'Background Color' ),
						},
						{
							value: textColor,
							onChange: ( colorValue ) => setAttributes( { textColor: colorValue } ),
							label: __( 'Text Color' ),
						},
					] }
				>
				</PanelColorSettings>
				
			</InspectorControls>
		),
		<ButtonEdit 
			onChange={ (text) => setAttributes( { text } ) }
			isSelected={ isSelected }
			align={ align } size={ size } color={ textColor } text={ text }  borderRadius={ cornerButtonRadius } />,
		isSelected && (
			<form
				onSubmit={ ( event ) => event.preventDefault() }
				className={ `blocks-button__inline-link` }>
				<Dashicon icon={ 'admin-links' } />
				<URLInput
					value={ url }
					onChange={ ( value ) => setAttributes( { url: value } ) }
				/>
				<IconButton
					icon={ 'editor-break' }
					label={ __( 'Apply' ) }
					type={ 'submit' }
				/>
			</form>
		),
	];
}
/*
<span key={ 'button' }
			className={ `ugb-button ugb-button-${align} ugb-button-${size}` }
			style={ {
				backgroundColor: color,
				borderRadius: cornerButtonRadius + 'px',
			} }>
			<RichText
				tagName={ 'span' }
				placeholder={ __( 'Enter Text' ) }
				value={ text }
				onChange={ (text) => setAttributes( { text: text } ) }
				formattingControls={ [ 'bold', 'italic', 'strikethrough' ] }
				className={ `ugb-button-inner` }
				style={ { color: textColor } }
				isSelected={ isSelected }
				keepPlaceholderOnFocus
			/>
		</span>
		*/
export const save = ( props ) => {

	const { url, text, align, color, textColor, size, cornerButtonRadius } = props.attributes;

	return (
		<ButtonSave align={ align } size={ size } url={ url } color={ textColor } text={ text } backgroundColor={ color } borderRadius={ cornerButtonRadius } />
	);
}

export const schema = {
	url: {
		type: 'string',
		source: 'attribute',
		selector: 'a',
		attribute: 'href',
	},
	text: {
		type: 'array',
		source: 'children',
		selector: 'a',
	},
	align: {
		type: 'string',
		default: 'center',
	},
	color: {
		type: 'string',
		default: '#2091e1',
	},
	textColor: {
		type: 'string',
		default: '#ffffff',
	},
	size: {
		type: 'string',
		default: 'normal',
	},
	cornerButtonRadius: {
		type: 'number',
		default: 4,
	}
}


/**
 * Register: Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'ugb/button', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Button' ), // Block title.
	icon: ButtonIcon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	description: 'Add customize button',
	category: 'stackable-ultimate-gutenberg-blocks', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'Button' ),
		__( 'Stackable' ),
	],
	attributes: schema,

	deprecated: [
        {
			attributes: deprecatedSchema_0_7,
			migrate: attributes => {
                return omit( merge( attributes, { align: attributes.textAlignment } ), ['textAlignment'] )
            },
			save: deprecatedSave_0_7,
        }
    ],

	edit,
	save,
} );
