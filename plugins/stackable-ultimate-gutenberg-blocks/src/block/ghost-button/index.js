/**
 * BLOCK: Ghost Button Block.
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';
import { GhostButtonIcon } from '../../icons'
import { deprecatedSchema_0_7, deprecatedSave_0_7 } from './deprecated'

import {
	registerBlockType,
	__,
	InspectorControls,
	BlockControls,
	RichText,
	BlockAlignmentToolbar,
	ColorPalette,
	PanelColorSettings,
	IconButton,
	Dashicon,
	SelectControl,
	RangeControl,
	URLInput,
	PanelBody,
	omit,
	merge,
} from '../../wp-imports'

export const edit = ( props ) => {
	const {
		isSelected,
		setAttributes
	} = props

	const {
		url,
		text,
		color,
		size,
		align,
		cornerButtonRadius,
		borderThickness
	} = props.attributes

	const buttonSizes = [
		{ value: 'small', label: __( 'Small' ) },
		{ value: 'normal', label: __( 'Normal ' ) },
		{ value: 'medium', label: __( 'Medium' ) },
		{ value: 'large', label: __( 'Large' ) },
	]

	return [
		isSelected && (
			<BlockControls>
				<BlockAlignmentToolbar
					value={ align }
					onChange={ align =>  {
						setAttributes( { align } );
					} }
					controls={ [ 'left', 'center', 'right', 'full' ] }
				/>
			</BlockControls>
		),
		<span key='button'
			className={ `ugb-button ugb-ghost-button ugb-button-${align} ugb-button-${size}` }
			style={ {
				borderColor: color,
				borderRadius: cornerButtonRadius + 'px',
				borderWidth: borderThickness + 'px',
			} } >
			<RichText
				tagName={ 'span' }
				placeholder={ __( 'Enter Text' ) }
				value={ text }
				onChange={ ( text ) => setAttributes( { text: text } ) }
				formattingControls={ [ 'bold', 'italic', 'strikethrough' ] }
				className={ `ugb-button-inner` }
				style={ { color } }
				isSelected={ isSelected }
				keepPlaceholderOnFocus
			/>
			{
				isSelected &&
				<InspectorControls key='inspector'>
					<PanelBody>
						<SelectControl
							label={ __( 'Size' ) }
							value={ size }
							options={ buttonSizes.map( ( { value, label } ) => ( {
								value: value,
								label: label,
							} ) ) }
							onChange={ ( newSize ) => { setAttributes( { size: newSize } ) } }
						/>
						<RangeControl
							label={ __( 'Corner Radius' ) }
							value={ cornerButtonRadius }
							min='1'
							max='50'
							onChange={ ( cornerRad ) => setAttributes( { cornerButtonRadius: cornerRad } ) }
						/>
						<RangeControl
							label={ __( 'Border Thickness' ) }
							value={ borderThickness }
							min='1'
							max='10'
							onChange={ ( borderThick ) => setAttributes( { borderThickness: borderThick } ) }
						/>
					</PanelBody>
					<PanelColorSettings
						title={ __( 'Color Settings' ) }
						colorSettings={ [
							{
								value: color,
								onChange: ( colorValue ) => setAttributes( { color: colorValue } ),
								label: __( 'Button Color' ),
							},
						] }
					>
					</PanelColorSettings>
				</InspectorControls>
			}
		</span>,
		isSelected && (
			<form
				key={ 'form-link' }
				onSubmit={ ( event ) => event.preventDefault() }
				className={ `blocks-button__inline-link`}>
				<Dashicon icon={ 'admin-links' } />
				<URLInput
					value={ url }
					onChange={ ( value ) => setAttributes( { url: value } ) }
				/>
				<IconButton
					icon={ 'editor-break' }
					label={ __( 'Apply' ) }
					type={ 'submit' }
				/>
			</form>
		),
	]
}

export const save = ( props ) => {

	const {
		url,
		text,
		align,
		color,
		size,
		cornerButtonRadius,
		borderThickness
	} = props.attributes;

	const buttonStyle = {
		borderColor: color,
		borderRadius: cornerButtonRadius + 'px',
		borderWidth: borderThickness + 'px',
	}

	return (
		<div className={ `ugb-button ugb-ghost-button ugb-button-${align} ugb-button-${size}` } style={ buttonStyle }>
			<a href={ url } className={ `ugb-button-inner` } style={ { color } }>
				{ text }
			</a>
		</div>
	);
}

const schema = {
	url: {
		type: 'string',
		source: 'attribute',
		selector: 'a',
		attribute: 'href',
	},
	text: {
		type: 'array',
		source: 'children',
		selector: 'a',
	},
	align: {
		type: 'string',
		default: 'center',
	},
	color: {
		type: 'string',
	},
	textColor: {
		type: 'string',
	},
	size: {
		type: 'string',
		default: 'normal',
	},
	cornerButtonRadius: {
		type: 'number',
		default: '4',
	},
	borderThickness: {
		type: 'number',
		default: '1',
	}
}

/**
 * Register: Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @param  {string}  name     Block name.
 * @param  {Object}  settings Block setting.
 * @return {?WPBlock}		  The block, if it has been successfully
 *							  registered; otherwise `undefined`.
 */
registerBlockType( 'ugb/ghost-button', {
	// Block name. Block names must be string that contains namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Ghost Button' ), // Block title.
	icon: GhostButtonIcon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'stackable-ultimate-gutenberg-blocks', // Block category - Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'Ghost Button' ),
		__( 'Stackable' ),
	],
	attributes: schema,

	deprecated: [
        {
			attributes: deprecatedSchema_0_7,
			migrate: attributes => {
                return omit( merge( attributes, { align: attributes.textAlignment } ), ['textAlignment'] )
            },
			save: deprecatedSave_0_7,
        }
    ],

	edit,
	save,
} )
