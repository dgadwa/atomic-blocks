import classnames from 'classnames';

import {
	__,
} from '../../wp-imports'

export const deprecatedSave_0_7 = ( props ) => {

	const {
		url,
		buttonURL,
		buttonText,
		buttonColor,
		buttonTextColor,
		cornerButtonRadius,
		size,
		title,
		titleColor,
		subtitle,
		subtitleColor,
		contentAlign,
		id,
		backgroundColor,
		opacity
	} = props.attributes

	const buttonStyle = {
		backgroundColor: buttonColor,
		color: buttonTextColor,
		borderRadius: cornerButtonRadius + 'px',
	}

	const style = url ? { backgroundImage: `url(${ url })` } : undefined

	const imageClass = url ? 'has-image' : ''

	const opacityClass = classnames(
						opacityToClass( opacity ),
						{
							'overlay-opacity': opacity !== 0,
						}
					);

	const displayNone = ( ! title && ! subtitle && ! buttonText ) ? 'has-no-content' : 'has-content'

	return (
		<div className={ `ugb-header ${imageClass} ${displayNone}` }>
			<div className={ `ugb-header-overlay ${opacityClass}` }
				style={ { backgroundColor: backgroundColor } }>
			</div>
			<section
				key="preview"
				data-url={ url }
				style={ style }
				className={ 'ugb-header-section' }>
				{ title && !! title.length && (
					<h2 className={ 'ugb-header-title' } style={ { color: titleColor } }>
						{ title }
					</h2>
				) }
				{ subtitle && !! subtitle.length && (
					<p className={ 'ugb-header-subtitle' } style={ { color: subtitleColor } }>
						{ subtitle }
					</p>
				) }
				{ buttonText && !! buttonText.length && (
					<a
						href={ buttonURL }
						className={ `wp-ugb-button ugb-button-${size}` }
						style={ buttonStyle }>
						{ buttonText }
					</a>
				) }
			</section>
		</div>
	);
}

function opacityToClass( ratio ) {
	return ( ratio === 0 ) ?
		null :
		'overlay-opacity-' + ( 1 * Math.round( ratio / 1 ) );
}

export const deprecatedSchema_0_7 = {
	title: {
		type: 'array',
		source: 'children',
		selector: 'h2',
		default: __( 'Heading Title' )
	},
	subtitle: {
		type: 'array',
		source: 'children',
		selector: 'p',
		default: __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus congue tincidunt nisit ut pretium. Duis blandit, tortor et suscipit tincidunt, dolor metus mattis neque, ac varius magna nibh ac tortor.' )
	},
	url: {
		type: 'string',
		source: 'attribute',
		selector: '.ugb-header .ugb-header-section',
		attribute: 'data-url',
	},
	buttonURL: {
		type: 'string',
		source: 'attribute',
		selector: 'a',
		attribute: 'href',
	},
	titleColor: {
		type: 'string',
		default: '#ffffff',
	},
	subtitleColor: {
		type: 'string',
		default: '#ffffff',
	},
	buttonText: {
		type: 'array',
		source: 'children',
		selector: '.ugb-header a.wp-ugb-button',
		default: __( 'Button' )
	},
	buttonColor: {
		type: 'string',
		default: '#2091e1',
	},
	buttonTextColor: {
		type: 'string',
		default: '#ffffff',
	},
	size: {
		type: 'string',
		default: 'normal',
	},
	cornerButtonRadius: {
		type: 'number',
		default: 4,
	},
	contentAlign: {
		type: 'string',
		default: 'center',
	},
	id: {
		type: 'number',
	},
	backgroundColor: {
		type: 'string',
		default: '#000000',
	},
	opacity: {
		type: 'number',
		default: 5,
	},
}