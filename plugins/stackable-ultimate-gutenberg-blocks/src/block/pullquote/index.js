/**
 * BLOCK: Pullquote
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';
import { QuoteIcon } from '../../icons'
import { deprecatedSchema_0_7, deprecatedSave_0_7 } from './deprecated'

import {
	registerBlockType,
	__,
	PanelColorSettings,
	InspectorControls,
	RichText,
	ColorPalette,
	omit,
	merge,
} from '../../wp-imports'

export const edit = ( props ) => {
	const onSetActiveEditable = ( newEditable ) => () => {
		setState( { editable: newEditable } )
	}

	const { isSelected, setAttributes, className } = props

	const { color, text, quoteColor } = props.attributes


	return [
		<blockquote
			key={ 'quote' }
			className={ 'ugb-pullquote' }
			style={ { '--quote-color': quoteColor } }>
			<RichText
				tagName={ 'p' }
				className={ 'ugb-pullquote-text' }
				value={ text }
				onChange={ ( nextValue ) => setAttributes( { text: nextValue } ) }
				placeholder={ __( 'Write quote…' ) }
				formattingControls={ [ 'bold', 'italic', 'strikethrough', 'link' ] }
				isSelected={ isSelected }
				keepPlaceholderOnFocus
				style={ {
					color: color
				} }
			/>
		</blockquote>,
		isSelected && (
			<InspectorControls>
				<PanelColorSettings
					title={ __( 'Color Settings' ) }
					colorSettings={ [
						{
							value: color,
							onChange: ( colorValue ) => setAttributes( { color: colorValue } ),
							label: __( 'Text Color' ),
						},
						{
							value: quoteColor,
							onChange: ( colorValue ) => setAttributes( { quoteColor: colorValue } ),
							label: __( 'Quote Color' ),
						},
					] }
				>
				</PanelColorSettings>
			</InspectorControls>
		)
	];
}

export const save = ( props ) => {

	const { color, text, quoteColor } = props.attributes

	return (
		<blockquote
			className={ 'ugb-pullquote' }
			style={ { '--quote-color': quoteColor } }>
			<p style={ { color: color } }>{ text }</p>
		</blockquote>
	);
}

export const schema = {
	text: {
		type: 'array',
		source: 'children',
		selector: 'p',
		default: __( 'It\'s okay to acknowledge that life can get complicated, but we musn\'t forget the beauty in its simplicity, too. From the multitude of stars above, to freshly mowed grass in the morning, life is simply wonderful.' )
	},
	color: {
		type: 'string',
		default: '',
	},
	quoteColor: {
		type: 'string',
		default: '',
	}
}


/**
 * Register: Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'ugb/pullquote', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Pullquote' ), // Block title.
	icon: QuoteIcon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'stackable-ultimate-gutenberg-blocks', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'Pullquote' ),
		__( 'Stackable' ),
	],
	attributes: schema,

	deprecated: [
        {
			attributes: deprecatedSchema_0_7,
			migrate: attributes => {
                return omit( merge( attributes, { quoteColor: attributes.borderColor } ), ['borderColor'] )
            },
			save: deprecatedSave_0_7,
        }
	],

	edit,
	save,
} );
