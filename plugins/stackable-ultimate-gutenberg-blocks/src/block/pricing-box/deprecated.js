export const deprecatedSave_0_7 = ( props ) => {
	const {
		url,
		url2,
		url3,
		pricingBoxTitle,
		pricingBoxTitle2,
		pricingBoxTitle3,
		price,
		price2,
		price3,
		perMonthLabel,
		perMonthLabel2,
		perMonthLabel3,
		buttonText,
		buttonText2,
		buttonText3,
		featureList,
		featureList2,
		featureList3,
		pricingBoxColor,
		priceColor,
		perMonthLabelColor,
		buttonColor,
		buttonTextColor,
		featureListColor,
		columns,
		size,
		cornerButtonRadius,
	} = props.attributes;

	const buttonStyle = {
		backgroundColor: buttonColor,
		color: buttonTextColor,
		borderRadius: cornerButtonRadius + 'px',
	}

	return (
		<div className={ `ugb-pricing-box column-${columns}` }>
			<div className={ 'ugb-pricing-box-column-one' }>
				{ pricingBoxTitle && !! pricingBoxTitle.length && (
					<h3 style={ { color: pricingBoxColor } }>
						{ pricingBoxTitle }
					</h3>
				) }
				{ price && !! price.length && (
					<p className={ 'ugb-pricing-box-pricing' } style={ { color: priceColor } }>
						{ price }
					</p>
				) }
				{ perMonthLabel && !! perMonthLabel.length && (
					<p className={ 'ugb-pricing-box-per-month-label' } style={ { color: perMonthLabelColor } }>
						{ perMonthLabel }
					</p>
				) }
				{ buttonText && !! buttonText.length && (
					<a
						href={ url }
						className={ `wp-ugb-button ugb-button-${size}` }
						style={ buttonStyle }>
						{ buttonText }
					</a>
				) }
				{ featureList && !! featureList.length && (
					<p className={ 'ugb-pricing-box-feature-list' } style={ { color: featureListColor } }>
						{ featureList }
					</p>
				) }
			</div>
			{ columns > 1 && (
				<div className={ 'ugb-pricing-box-column-two' }>
					{ pricingBoxTitle2 && !! pricingBoxTitle2.length && (
						<h3 style={ { color: pricingBoxColor } }>
							{ pricingBoxTitle2 }
						</h3>
					) }
					{ price2 && !! price2.length && (
						<p className={ 'ugb-pricing-box-pricing' } style={ { color: priceColor } }>
							{ price2 }
						</p>
					) }
					{ perMonthLabel2 && !! perMonthLabel2.length && (
						<p className={ 'ugb-pricing-box-per-month-label' } style={ { color: perMonthLabelColor } }>
							{ perMonthLabel2 }
						</p>
					) }
					{ buttonText2 && !! buttonText2.length && (
						<a
							href={ url2 }
							className={ `wp-ugb-button ugb-button-${size}` }
							style={ buttonStyle }>
							{ buttonText2 }
						</a>
					) }
					{ featureList2 && !! featureList2.length && (
						<p className={ 'ugb-pricing-box-feature-list' } style={ { color: featureListColor } }>
							{ featureList2 }
						</p>
					) }
				</div>
			) }
			{ columns > 2 && (
				<div className={ 'ugb-pricing-box-column-three' }>
					{ pricingBoxTitle3 && !! pricingBoxTitle3.length && (
						<h3 style={ { color: pricingBoxColor } }>
							{ pricingBoxTitle3 }
						</h3>
					) }
					{ price3 && !! price3.length && (
						<p className={ 'ugb-pricing-box-pricing' } style={ { color: priceColor } }>
							{ price3 }
						</p>
					) }
					{ perMonthLabel3 && !! perMonthLabel3.length && (
						<p className={ 'ugb-pricing-box-per-month-label' } style={ { color: perMonthLabelColor } }>
							{ perMonthLabel3 }
						</p>
					) }
					{ buttonText3 && !! buttonText3.length && (
						<a
							href={ url3 }
							className={ `wp-ugb-button ugb-button-${size}` }
							style={ buttonStyle }>
							{ buttonText3 }
						</a>
					) }
					{ featureList3 && !! featureList3.length && (
						<p className={ 'ugb-pricing-box-feature-list' } style={ { color: featureListColor } }>
							{ featureList3 }
						</p>
					) }
				</div>
			) }
		</div>
	);
}