import {
	__,
} from '../../wp-imports'

export const deprecatedSave_0_7 = ( props ) => {

	const {
		heading,
		tagline,
		des,
		mediaURL,
		mediaID,
		headingColor,
		taglineColor,
		desColor,
		buttonURL,
		buttonText,
		buttonColor,
		buttonTextColor,
		size,
		cornerButtonRadius,
		contentAlign
	} = props.attributes;

	const buttonStyle = {
		backgroundColor: buttonColor,
		color: buttonTextColor,
		borderRadius: cornerButtonRadius + 'px',
	}

	const imageClass = mediaURL ? 'has-image' : ''

	const displayNone = ( ! heading && ! tagline && ! des && ! buttonText ) ? 'has-no-content' : 'has-content'

	return (
		<div className={ `ugb-card ${imageClass} ${displayNone}` }>
			{ mediaURL && <div className="ugb-card-image-container" style={{ backgroundImage: `url(${mediaURL})`, textAlign: contentAlign }} data-src={mediaURL}></div> }
			{ heading && !! heading.length && (
				<h4 style={ { color: headingColor, textAlign: contentAlign } }>
					{ heading }
				</h4>
			) }
			{ tagline && !! tagline.length && (
				<p className={ 'ugb-tagline' } style={ { color: taglineColor, textAlign: contentAlign } }>
					{ tagline }
				</p>
			) }
			{ des && !! des.length && (
				<p className={ 'ugb-card-des' } style={ { color: desColor, textAlign: contentAlign } }>
					{ des }
				</p>
			) }
			{ buttonText && !! buttonText.length && (
				<a
					href={ buttonURL }
					className={ `wp-ugb-button wp-block-button ugb-button-${size} ugb-button-${contentAlign}` }
					style={ buttonStyle }>
					{ buttonText }
				</a>
			) }
		</div>
	);
}

export const deprecatedSchema_0_7 = {
	mediaID: {
		type: 'number',
	},
	mediaURL: {
		type: 'string',
		source: 'attribute',
		selector: '.ugb-card-image-container',
		attribute: 'data-src',
	},
	heading: {
		type: 'array',
		source: 'children',
		selector: '.ugb-card h4',
		default: __( 'Ben Adams' )
	},
	tagline: {
		type: 'array',
		source: 'children',
		selector: '.ugb-tagline',
		default: __( 'Ben is the head of our small team' )
	},
	des: {
		type: 'array',
		source: 'children',
		selector: '.ugb-card-des',
		default: __( 'Ben is the head of our small team. He loves walking his dog, Walter, when he has some free time.' )
	},
	headingColor: {
		type: 'string',
	},
	taglineColor: {
		type: 'string',
	},
	desColor: {
		type: 'string',
	},
	buttonURL: {
		type: 'string',
		source: 'attribute',
		selector: '.wp-ugb-button',
		attribute: 'href',
	},
	buttonText: {
		type: 'array',
		source: 'children',
		selector: '.wp-block-button',
		default: __( 'Button' )
	},
	buttonColor: {
		type: 'string',
		default: '#2091e1',
	},
	buttonTextColor: {
		type: 'string',
		default: '#ffffff',
	},
	size: {
		type: 'string',
		default: 'normal',
	},
	cornerButtonRadius: {
		type: 'number',
		default: 4,
	},
	contentAlign: {
		type: 'string',
		default: 'left',
	},
}